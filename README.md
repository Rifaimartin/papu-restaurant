![Preview-Screens](https://gitlab.com/academyyyy12/papu-restaurant/blob/master/src/assets/top_logo.svg)

## Tech stack 🛠

- [ReactJS](https://reactjs.org) via [create-react-app](https://github.com/facebook/create-react-app) - web rendering and PWA functionality in one neat package
- [React Native](https://facebook.github.io/react-native/) - native rendering
- [React Native Web](https://github.com/necolas/react-native-web) - the most important module, that makes all the magic hapen.
- [React Navigation](https://reactnavigation.org/) - navigation framework for the native app
- [React Router](https://reacttraining.com/react-router/web) - navigation framework for the web app
- [React Redux](https://redux.js.org/) - state managment
- [React Saga](https://redux-saga.js.org/) - redux middleware side effects manager
- [Moltin](https://moltin.com) - e-commerce platform offering great backend with fantastic API



## To do 📋

- bug fixes



