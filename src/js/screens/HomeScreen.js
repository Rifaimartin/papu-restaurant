import React, { Component } from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Card from '../components/Card'

class HomeScreen extends Component {
  render() {
    return (
      <ScrollView
        style={styles.main}
        contentContainerStyle={styles.scrollContainer}
      >
        <Card
          title="Nasi Goreng"
          copy="Lorem Ipsum"
          bgColor="rgba(233,201,004,0.9)"
          image={require('../../assets/nasi-goreng-saus-tiram.jpg')}
          onPress={() => this.props.navigation.navigate('Menu')}
        />
        <Card
          title="Soto Papu"
          copy="Lorem Ipsum"
          bgColor="rgba(104,201,32,0.9)"
          image={require('../../assets/soto.jpg')}
        />
        <Card
          title="Sering Di order"
          copy="Lorem Ipsum"
          bgColor="rgba(9,172,235,0.9)"
          image={require('../../assets/card4.png')}
        />
        <Card
          title="Ayam Geprek"
          copy="Lorem Ipsum"
          bgColor="rgba(238,21,40,0.9)"
          image={require('../../assets/ayam.jpg')}
        />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#fff'
  },
  scrollContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'center',
    alignContent: 'flex-start'
  }
})

HomeScreen.propTypes = {
  navigation: PropTypes.object
}

export default HomeScreen
